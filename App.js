import { StatusBar } from 'expo-status-bar';
import React,{Component} from 'react';
import { StyleSheet, Text, View,ActivityIndicator } from 'react-native';
import { NavigationContainer} from "@react-navigation/native";
import "react-native-gesture-handler";
import {createStackNavigator} from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import LoginScreen from './screen/LoginScreen';
import RegisterScreen from './screen/RegisterScreen';
import HomeScreen from './screen/HomeScreen';
import Annonce from './screen/Annonce';
import Contact from './screen/Contact';
import   Icon  from 'react-native-vector-icons/Ionicons';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import { auth } from './Firebase';
import Dashboard from './screen/Dashboard';
import NewAnnonce from './screen/NewAnnonce';
import Profile from './screen/Profile';
import Photo from './screen/Photo';
import Trajet from './screen/Trajet';
import Choose from './screen/Choose';

import { LogBox } from 'react-native';
import ListTrajet from './screen/ListTrajet';
import TrajetDetail from './screen/TrajetDetail';
import ListAnnonce from './screen/ListAnnonce';
import AnnonceDetail from './screen/AnnonceDetail';

////import LogBox  from 'react-native-web/dist/index'
//LogBox.ignoreLogs(['Setting a timer']);



const Drawer = createDrawerNavigator();
const HomeStack = createStackNavigator();
const AnnonceStack = createStackNavigator();
const ContactStack = createStackNavigator();
const DashboardStack = createStackNavigator();
const NewAnnonceStack = createStackNavigator();
const Stack = createStackNavigator();

const HomeStackScreen=({navigation})=>
(
  
<HomeStack.Navigator  screenOptions={globalScreenOptions} > 
 
 <HomeStack.Screen   name="Home" component={HomeScreen} options={
  {
   title:"Mycolis",
   headerLeft:()=>(
     <View>
     <Icon.Button name="menu" size={25}
     backgroundColor="#955f31" onPress={()=>navigation.openDrawer()}></Icon.Button>
     
</View>
   )}}   />
   <Stack.Screen options={{ title:" Detail des Annonces "}} name="AnnonceDetail" component={AnnonceDetail}  />
  
 
</HomeStack.Navigator> 
)

const AnnonceStackScreen=({navigation})=>(
 
  <AnnonceStack.Navigator screenOptions={globalScreenOptions} > 

  
  <AnnonceStack.Screen name="Annonce" component={Annonce} options={{title:"Annonce", headerLeft:()=>(
     <View>
     <Icon.Button name="ios-menu" size={25}
      backgroundColor="#955f31" onPress={()=>navigation.openDrawer()}></Icon.Button>
</View>)}} />

  </AnnonceStack.Navigator> 
)

const DashboardStackScreen=({navigation})=>(
 
  <DashboardStack.Navigator screenOptions={globalScreenOptions} > 

  
  <DashboardStack.Screen name="Dashboard" component={Dashboard} options={{ headerLeft:()=>(
     <View>
     <Icon.Button name="ios-menu" size={25}
      backgroundColor="#955f31" onPress={()=>navigation.openDrawer()}></Icon.Button>
</View>)}} />
<Stack.Screen name="New" component={NewAnnonce}/> 
<Stack.Screen name="profile" component={Profile}/> 
<Stack.Screen  name="photo" component={Photo} />
<Stack.Screen  name="trajet" component={Trajet} />
<Stack.Screen  name="choose" component={Choose} />
<Stack.Screen  name="ListTrajet" component={ListTrajet} />
<Stack.Screen  name="ListAnnonce" component={ListAnnonce} />
<Stack.Screen  name="TrajetDetail" component={TrajetDetail} />
<Stack.Screen options={{ title:" Detail des Annonces "}} name="AnnonceDetail" component={AnnonceDetail} />
  </DashboardStack.Navigator> 
  
)

const ContactStackScreen=({navigation})=>(
 
  <ContactStack.Navigator screenOptions={globalScreenOptions} > 

  
  <ContactStack.Screen name="about" component={Contact} options={{title:"contact", headerLeft:()=>(
     <View>
     <Icon.Button name="ios-menu" size={25}
      backgroundColor="#955f31" onPress={()=>navigation.openDrawer()}></Icon.Button>
</View>)}} />

  </ContactStack.Navigator> 
)

const globalScreenOptions={
headerStyle:{backgroundColor:"#955f31"},
headerTitleStyle:{color:"white"},
headerTintColor:"white",


}

const DrawerStackScreen=()=>(

   <Drawer.Navigator initialRouteName="Home">
    
    
    <Drawer.Screen name="Accueils" component={HomeStackScreen} options={{ headerLeft:()=>(
     
     <Icon name="camera-reverse " size={20}
      backgroundColor="#955f31" ></Icon>

)}}/>
   
    <Drawer.Screen name="Annonce"  component={AnnonceStackScreen}/>
    <Drawer.Screen name="Contact" component={ContactStackScreen}/>
    <Drawer.Screen name="Tableau de bord" component={DashboardStackScreen} />
   
   </Drawer.Navigator> 
 
) 


export class  App extends Component {

 constructor(props){
  super(props);
   this.state={
    loaded:false,
    login:false,
}
//this.navigation;
  }

  componentDidMount()  {
 
    const onAuthStateChangedUnsubscribe =    auth.onAuthStateChanged(async (User)=>{
      console.log(User);
       if(!User){
       
         this.setState({login:false,loaded:true})
         
         
         }
      
         else{
           
           // -> Alert Email Verification
         // await User.sendEmailVerification()
           const onIdTokenChangedUnsubscribe = auth.onIdTokenChanged((User) => {
             const unsubscribeSetInterval = setTimeout(() => {
             // auth.currentUser.reload();
             
               auth.currentUser.getIdToken(/* forceRefresh */ true);
             }, 10000);

             if (User && User.emailVerified) {
               clearInterval(unsubscribeSetInterval) //delete interval
               onAuthStateChangedUnsubscribe() //unsubscribe onAuthStateChanged
             
              
               // -> Go to your screnn
               return   onIdTokenChangedUnsubscribe() //unsubscribe onIdTokenChanged
             }


           })
           
           this.setState({login:true ,loaded:true}); 
         }
        })
     };

     


 render(){
const {login,loaded}=this.state;
  
if(!loaded){
  return(
    <View>
   <ActivityIndicator size="small" color="#0000ff"/>
   </View>
   )
}
if(!login){
  return(
  <NavigationContainer>
  <Stack.Navigator  screenOptions={globalScreenOptions}> 

 <Stack.Screen options={{ title:"Lets Sign In"}} name="Login" component={LoginScreen} />
 <Stack.Screen options={{ title:"Lets Sign Up"}} name="register" component={RegisterScreen} />

 <Stack.Screen name="home" component={DrawerStackScreen} options={{ headerShown:false}} />
 
  </Stack.Navigator> 
  </NavigationContainer>
  )
}

return(

<NavigationContainer>


<Drawer.Navigator initialRouteName="Accueil">

 <Drawer.Screen name="Accueil" component={HomeStackScreen} />
 <Drawer.Screen name="Annonce"  component={AnnonceStackScreen}/>
 
 <Drawer.Screen name="Tableau de bord" component={DashboardStackScreen} />
 <Drawer.Screen name="Contact" component={ContactStackScreen}/>
 
</Drawer.Navigator> 
</NavigationContainer>
)

}
}

export  default  App
/*















<Stack.Navigator  screenOptions={globalScreenOptions}> 

<Stack.Screen 
options={{
  title:"Lets Sign UP"
}}
name="Login" component={LoginScreen} 
 />
 <Stack.Screen 
options={{
  title:"Lets Sign UP"
}}
name="register" component={RegisterScreen}
 />
 <Stack.Screen   name="home" component={HomeScreen} />
 
</Stack.Navigator > 





 if(!login){
    return(
    <NavigationContainer>
    <Stack.Navigator  screenOptions={globalScreenOptions}> 

   <Stack.Screen options={{ title:"Lets Sign In"}} name="Login" component={LoginScreen} />
   <Stack.Screen options={{ title:"Lets Sign Up"}} name="register" component={RegisterScreen} />
   <Stack.Screen name="home" component={DrawerStackScreen} options={{ headerShown:false}}  />
   
    </Stack.Navigator> 
    </NavigationContainer>
    )
  }

return(

 <NavigationContainer>
 

  <Drawer.Navigator initialRouteName="Accueil">

   <Drawer.Screen name="Accueil" component={HomeStackScreen}/>
   <Drawer.Screen name="Annonce"  component={AnnonceStackScreen}/>
   
   <Drawer.Screen name="Tableau de bord" component={DashboardStackScreen}/>
   <Drawer.Screen name="Contact" component={ContactStackScreen}/>
   
  </Drawer.Navigator> 
  </NavigationContainer>
)
  



*/