import React,{useState,useEffect} from 'react';
import {Modal, StyleSheet, Text,TouchableOpacity, View,Button,Platform, ScrollView ,TextInput,Image,ActivityIndicator} from 'react-native';
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import   Icon  from 'react-native-vector-icons/Ionicons';
import DateTimePickerModal from "react-native-modal-datetime-picker"   ;
import {Input} from "react-native-elements";
import * as ImagePicker from 'expo-image-picker';
import {Picker} from '@react-native-picker/picker';
import { Formik } from 'formik';
import * as yup from 'yup';
import { auth,db } from '../Firebase';
import firebase from "firebase";
 require("firebase/firebase-storage");

const NewAnnonce = ({navigation}) => {
    const [Visible, setVisibility] = useState(false);
     const [date,setDate]=useState(moment());
     const [dateDisplay,setDateDisplay]=useState(moment());
     const [show,setShow]=useState(false);
     const [hasGalleryPermission, setHasGalleryPermission] = useState(null);
     const [image, setImage] = useState("https://kerala.nic.in/wp-content/themes/sdo-theme/images/default/image-gallery.jpg");
     const [taille,setTaille]=useState("");
     const [villeDep,setVilleDep]=useState("");
     const [villeAr,setVilleAr]=useState("");
     const [prix,setPrix]=useState("");
     const [desc,setDesc]=useState("");
     const [titre,setTitre]=useState("");
//
//https://www.phoca.cz/images/projects/phoca-gallery-r.png
     useEffect(() => {
      (async () => {
       
        const galleryStatus = await ImagePicker.requestMediaLibraryPermissionsAsync();
        setHasGalleryPermission(galleryStatus.status === 'granted');
  
      })();   
    }, []);
  
    
  
    const pickImage = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });
      console.log(result);
  
      if (!result.cancelled) {
        setImage(result.uri);
      }
    };
  
  
    if ( hasGalleryPermission === false) {
      return <View />;
    }
    if ( hasGalleryPermission === false) {
      return <Text>No access to camera</Text>;
    }


   const onChange=(e,selectedDate) =>{
     setDateDisplay(moment(selectedDate));
   }
    
  


   const handleConfirm=(date)=>{
     setDateDisplay(date.toUTCString())
     setVisibility(false)
  }

 const onPressCancel=()=>{
    setVisibility(false)
  }
  
 const onPressButton=()=>{
  setVisibility(true)
}

 const uploadPhotoAsync= async (url)=>{
   const path=`photos/${auth.currentUser.uid}/${Date.now()}.jpg`
 return new Promise(async(res,rej)=>{
   const response=await fetch(url);
   const file= await response.blob();
   let upload = firebase.storage().ref(path).put(file);
   //db.storage().ref(path).put(file);
   upload.on(
     "state-changed",
     snapshot=>{},
     err=>{
      rej(err);
     },
     async ()=>{
       const url= await  upload.snapshot.ref.getDownloadURL();
       //upload.snapshot.ref.getDowloadURL();
       res(url);
      }
   );
 });
  };

const onAnnonce= async (values, actions)=>{
  
  const remoteUrl= await uploadPhotoAsync(image);
  alert(JSON.stringify(values));
       
  actions.setSubmitting(false);
  
   setTitre(values.titre);
   setPrix(values.prix);
   setDesc(values.desc);

      db.collection("annonces")
      .add({
               Iduser:auth.currentUser.uid, 
               date_depart:dateDisplay,
               ville_depart:villeDep,
               ville_arrivee:villeAr,
               taille:taille,
               image:remoteUrl,
               titre:values.titre,
               description:values.desc,
               prix:values.prix,
           })
           
           .then((res)=> {  
             
           
            navigation.navigate('ListAnnonce');})  
           .catch((error)=>
           console.log(error)
           ); 
           
           // 
           
};



const validation = yup.object().shape({
 
  //formikProps.handleSubmit
   //villeDep: yup.string().matches(phoneRegExp, "Numero de telephone  invalide").required("numero de telephone  8 chiffre").min(8,"Numero de telephone  8 chiffre"),
  
      titre: yup.string().required("champ obligatoire"),
      prix:  yup.string().required("champ obligatoire"),
      desc:  yup.string().required("champ obligatoire"),
     // date_depart:  yup.string().required("champ obligatoire"),
     // date_arrivee:  yup.string().required("champ obligatoire"),
    });
    
    return (

      <ScrollView  horizontal={false}  showsVerticalScrollIndicator={true} >
      <View style={styles.main}>
       <Formik
        initialValues={{titre:titre,prix:prix , desc:desc}}
      
        onSubmit={(values, actions)=> {
           onAnnonce(values,actions);
          // actions.resetForm();
    
        }}
      validationSchema={validation}
     >
         {(formikProps) => (
               <React.Fragment>
           <View  style={styles.elem}>

               <Text style={styles.txt}> Titre de l'annonce <Text style={{ color: 'red' }}>*</Text> </Text>
              <Input 
               placeholder="Entrez le titre " type="text"
               onChangeText={formikProps.handleChange('titre')}
               value={formikProps.values.titre}
               onBlur={formikProps.handleBlur('titre')}
               keyboardType="default"
               />

       <Text style={{ color: 'red',marginLeft:5 }}>{formikProps.errors.titre}</Text>
      </View>


      <View style={styles.elem}>

             <Text style={styles.txt}>  Ville de départ <Text style={{ color: 'red' }}>*</Text> </Text>
            

          <View style={styles.picker}>

      <Picker
      selectedValue={villeDep}
       onValueChange={(itemValue, itemIndex) =>
     
       setVilleDep(itemValue)
    
       }>
   
    

    <Picker.Item label="Java" value="java" />
    <Picker.Item label="JavaScript" value="js" />
    <Picker.Item label="cote d'ivoire" value="cote d'ivoire" />
    <Picker.Item label="JavaScript" value="js" />
    <Picker.Item label="Java" value="java" />
    <Picker.Item label="bresil" value="bresil" />
    <Picker.Item label="Java" value="java" />
    <Picker.Item label="JavaScript" value="js" />
    <Picker.Item label="Java" value="java" />
    <Picker.Item label="JavaScript" value="js" />
    <Picker.Item label="Java" value="java" />
    <Picker.Item label="JavaScript" value="js" />
    <Picker.Item label="Java" value="java" />
    <Picker.Item label="JavaScript" value="js" />
    <Picker.Item label="tunisie" value="tunisie" />
    <Picker.Item label="france" value="france" />
    </Picker>

    </View>




</View>

      <View  style={styles.elem}>

              <Text style={styles.txt}>  Ville d'arrivée  <Text style={{ color: 'red' }}>*</Text></Text>
              
              <View style={styles.picker}> 
         <Picker
        selectedValue={villeAr}
         onValueChange={(itemValue, itemIndex) =>
         
         setVilleAr(itemValue)
        
        }>
      
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="cote d'ivoire" value="cote d'ivoire" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="bresil" value="bresil" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="tunisie" value="tunisie" />
        <Picker.Item label="france" value="france" />
        </Picker>

        </View>




      </View>
        

      <View  style={styles.elem} >
             <TouchableOpacity   onPress={() => pickImage()}>
            
             <Text style={styles.img}> Ajoutez une image <Text style={{ color: 'red' }}>*</Text></Text>
             <View style={{flexDirection:'row',marginBottom:25,marginTop:10}}>
            
             {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 ,marginLeft:5,borderRadius:5 }} />}
            <Icon
             style={{marginLeft:75,marginTop:25}}
              name="camera"
              size={75}
              color='black'
             />
             
               
          
              </View>
           </TouchableOpacity>

         
      </View>


      <View  style={styles.elem}>

          <Text style={styles.txt}> Taille <Text style={{ color: 'red' }}>*</Text></Text>
         
     <View style={styles.picker}>

    <Picker
        selectedValue={taille}
         onValueChange={(itemValue, itemIndex) =>
         
         setTaille(itemValue)
        
        }>
      
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="cote d'ivoire" value="cote d'ivoire" />
        <Picker.Item label="3 kg" value="3 kg" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="bresil" value="bresil" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="tunisie" value="tunisie" />
        <Picker.Item label="france" value="france" />
        </Picker>

        </View>




     </View>

      <View   style={styles.elem}>

            <Text style={styles.txt}>  Prix <Text style={{ color: 'red' }}>*</Text> </Text>
            <Input
            placeholder='Entrez le prix'
            onChangeText={formikProps.handleChange('prix')}
            value={formikProps.values.prix}
            onBlur={formikProps.handleBlur('prix')}
            keyboardType="numeric"
            leftIcon={
            <Icon
            name='calendar-outline'
            size={24}
            color='black'
            />
            }
            />

          <Text style={{ color: 'red',marginLeft:5 }}>{formikProps.errors.prix}</Text>
     </View>

      <View  style={styles.elem}>
             
             <Text style={styles.txt}>  Date de départ  <Text style={{ color: 'red' }}>*</Text> </Text>
             <TouchableOpacity
              activeOpacity={0}   onPress={onPressButton} >
             <Input
              style={styles.col}
              onFocus={onPressButton}
               onTextInput={onPressButton}
                placeholder='Entrez le prix'
                value={moment(dateDisplay).format('DD/MM/YYYY')}
                keyboardType="default"
            rightIcon={
             <Icon
             name='calendar-outline'
              size={24}
             color="#204d48"
              />
              }
              onChangeText={(text)=>setDateDisplay(text)}
             />
            
              <DateTimePickerModal
              isVisible={Visible}
             onConfirm={handleConfirm}
              onCancel={onPressCancel}
              mode="date"
             onDateChange={onChange}
          />
           </TouchableOpacity>

      </View>
     
      <View style={styles.textarea}>

            <Text style={styles.txt}> Description  <Text style={{ color: 'red' }}>*</Text> </Text>
            <TextInput
             style={styles.textinput}
             placeholder="Descrire l'objet"
             multiline={true}
             numberOfLines={5}
             onChangeText={formikProps.handleChange('desc')}
             value={formikProps.values.desc}
             onBlur={formikProps.handleBlur('desc')}
             keyboardType="default"
           />

      <Text style={{ color: 'red' ,marginLeft:5}}>{formikProps.errors.desc}</Text>
     </View>


   
     {formikProps.isSubmitting ? (
            <ActivityIndicator  size="large" color="#0000ff" />
          ) : (

     <View  style={styles.save}>
         <TouchableOpacity     onPress={formikProps.handleSubmit}  >   
             <Icon
             name='checkmark'
              size={70}
             color="#955f31"
             style={styles.iconsave}
              />
         </TouchableOpacity>
       
         
     </View>
      )}
     </React.Fragment>
       )}
      </Formik>
       </View>
       </ScrollView>
    );
  };

export default NewAnnonce

const styles = StyleSheet.create({
 picker:{
  borderBottomWidth:1,
  marginHorizontal:7,
  marginBottom:10,
  borderColor:'gray'
   },
   elem:{
     marginBottom:5,
   },
  main:{
   marginTop:40, 
   //backgroundColor:'#ECDDB6',
  },
  txt:{
    fontSize:17 ,
    marginBottom:15,
   },
   avatar: {
    width: 70,
    height: 70,
   borderRadius: 63,  
   borderWidth: 4,
  
  
   alignItems:'center',
   justifyContent:'center',
   
  },
   save:{
    justifyContent:'flex-end' ,
    alignItems:'flex-end',
    marginBottom:10,
    marginRight:4,
   },
   iconsave:{
    width: 75,
    height: 75,
   borderRadius: 63,  
   borderWidth: 4,
   borderColor: "#955f31",
   backgroundColor: "#f3d8a0",
    
   }, 
   img:{
    fontSize:17 ,
    marginTop:20,
    marginLeft:5,
    color:'gray',
   },
   col:{
    color:'gray', 
   
   },
   textarea:{
      marginBottom:10,
     
   },
   textinput:{
    marginTop:10,
    marginLeft:5,
    borderBottomWidth:1,
    borderTopWidth:1,
    marginHorizontal:3,
    borderLeftWidth:1,
    borderRightWidth:1,
    borderColor:'gray'
    
   },
in:{
    justifyContent:'center',
    alignItems:'center',
   
    marginTop:45,
},
btnText:{
  position:'absolute',
  top:0,
  height:42,
  paddingHorizontal:20,
  flexDirection:'row',
  alignItems:'center',
  justifyContent:'center',

},
btnCancel:{ 
  left:0
},
btnDone:{
  right:0,
}

})
/*

 <View>
     
        <TouchableOpacity
            activeOpacity={0}   onPress={onPressButton} >
     
         <View style={{marginHorizontal:45}}  onPress={onPressButton}>
             <View style={{paddingVertical:15,paddingHorizontal:10,borderColor:'gray',borderWidth:1 ,marginTop:70,flexDirection:'row' }}><Text>{moment(dateDisplay).format('DD/MM/YYYY')} </Text>
              <Icon name="calendar-outline" size={25} style={{marginLeft:100,paddingLeft:44}}  color="#204d48"></Icon>
            </View>
       
         </View>
         <DateTimePickerModal
           isVisible={Visible}
          onConfirm={handleConfirm}
          onCancel={onPressCancel}
           mode="date"
          onDateChange={onChange}
          />
     
       </TouchableOpacity>
      </View>











  <TouchableOpacity
         activeOpacity={0} onPress={()=>setShow(true)}>
     
      <View style={{marginHorizontal:45}}>
        <Text style={{paddingVertical:15,paddingHorizontal:10,borderColor:'gray',borderWidth:1 ,marginTop:70 }}>{date.format('MMMM Do, YYYY')}</Text>
      </View>
      <Modal
      transparent={true}  
      animationType='slide'
      visible={show}
      supportedOrientations={['portrait']}
      onRequestClose={()=>setShow(false)}
      >
        <View style={{flex:1}}>
         <TouchableOpacity style={{flex:1,alignItems:'flex-end',flexDirection:'row'}} activeOpacity={1} visible={show} onPress={()=>setShow(false)}>
      <TouchableOpacity underlayColor={'#fff'} style={{flex:1,borderTopColor:'#e9e9e9',borderTopWidth:1}}
      onPress={()=>console.log('datepicker clicked')}
      >
   <View style={{backgroundColor:'#fff',height:256,overflow:'hidden'}}>
   <View style={{marginTop:20}}>
     <DateTimePicker
      timeZoneOffsetInMinutes={0}
      value={new Date(date)}
      mode='date'
      minimumDate={new Date(moment().subtract(120,'years').format('YYYY-MM-DD'))}
      maximumDate={new Date(moment().format('YYYY-MM-DD'))}
      onChange={onChange}
     />
   </View>
     <TouchableOpacity underlayColor={'transparent'} onPress={onCancelPress}
     style={[styles.btnText,styles.btnCancel]}
      >
    <Text>ok </Text>
   </TouchableOpacity>

   <TouchableOpacity underlayColor={'transparent'} onPress={onCancelDone}
     style={[styles.btnText,styles.btnDone]}
      >
 <Text> Done</Text>
   </TouchableOpacity>
   </View>

      </TouchableOpacity>
         </TouchableOpacity>

        </View>


      </Modal>
       </TouchableOpacity>
*/ 