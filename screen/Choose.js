import React,{useState} from 'react';
import {Modal, StyleSheet, Text,TouchableOpacity, View,Button,Platform, ScrollView ,TextInput} from 'react-native';
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import   Icon  from 'react-native-vector-icons/Ionicons';
import DateTimePickerModal from "react-native-modal-datetime-picker"   ;
import {Input,Image} from "react-native-elements";


const Choose = ({navigation}) => {
    return (
        <View style={styles.main}>
            <TouchableOpacity  onPress={()=>navigation.navigate('New')} >
            <View   style={styles.ann}>
            <Text style={styles.txt}>Créer une annonce</Text>
                <Icon style={styles.ico}  name="add-circle-outline" size={40} color="#7f432f"></Icon>
                </View>
            </TouchableOpacity>

            <TouchableOpacity  onPress={()=>navigation.navigate('trajet')}>
            <View style={styles.tra}>
               <Text style={styles.txt}>Créer un Trajet</Text> 
                <Icon  style={styles.ico} name="airplane-outline" size={40} color="#7f432f"></Icon>
               
            </View>
            
            </TouchableOpacity>
        </View>
    )
}

export default Choose  

const styles = StyleSheet.create({
main:{
flex:1,
flexDirection:'column',
justifyContent:'center',
alignItems:'center',

},
ico:{
marginLeft:40,

},
txt:{
    fontSize:25,
    color:"#7f432f",
},
ann:{
  flexDirection:'row',
  borderBottomWidth:5,
  borderTopWidth:1,
  borderWidth:1,
  marginBottom:30,
  borderColor:"#d8b67d",
},
tra:{
    flexDirection:'row',
    borderBottomWidth:5 ,
    borderTopWidth:1,
    borderWidth:1,
    borderColor:"#d8b67d",
    shadowColor: "black",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 1
    },
}
})

