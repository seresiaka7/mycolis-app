import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, View,TouchableOpacity,Image,ScrollView } from 'react-native'
import {Button,Input,Avatar} from "react-native-elements";
import   Icon  from 'react-native-vector-icons/Ionicons';
import { Formik } from 'formik';
import * as yup from 'yup';
import { auth,db } from '../Firebase';








const Profile = ({navigation}) => {
    
     const [nom,setNom]= useState("sere");
     const [prenom,setPrenom]= useState("siaka");
     const [email,setEmail]= useState("seresiaka7@gmail.com");
     const [image,setImage]= useState("");
     const [tel,setTel]= useState("");
     const [password,setPassword]= useState("");
     const [confirmpassword,setConfirmpassword]= useState("");
     const [User,setUser]= useState("");



     const take = async () => {
       <View> bonjour sere seresiaka7</View>
      }



      const lregex=/(?=.*[a-z])/;
      const uregex=/(?=.*[A-Z])/;
      const nregex=/(?=.*[0-9])/;
      const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
      const validationSchema = yup.object().shape({
          nom: yup.string().required().label('nom'),
          prenom: yup.string().required().label('prenom'),
         
          tel: yup.string().matches(phoneRegExp, "Numero de telephone  invalide").required("Numero de telephone  à 8 chiffre").min(8,"Numero de telephone  8 chiffre"),
      
          email: yup.string().required('email obligatoire').email('email doit être valide '),
          password: yup.string().matches(lregex,"une lettre miniscule est obligatoire").matches(uregex,"une lettre majuscule  est obligatoire").matches(nregex,"une valeur  est obligatoire").required('password obligatoire').min(8,"8 caractères au minimum"),
          confirmpassword: yup.string().oneOf([yup.ref('password')],"les mots de passe doivent être egaux").required("password obligatoire")
          ,
        });


        useEffect(() => {
          db.collection("users")
          .doc(auth.currentUser.uid)
          .get()
          .then((snapshot)=>{
            if(snapshot.exists){
        
              setUser(snapshot.data());
             // setIsLoading(false);
            }
            else{
              console.log("does not exist");
            }
            });
           
           // setDate(Trajet.date_depart);
          
        }, []);
      

    return (
       
        <ScrollView  horizontal={false}  showsVerticalScrollIndicator={true} >
            <View style={styles.container} >
          <TouchableOpacity  style={styles.profile}  onPress={()=>navigation.navigate('photo')}>
          <Image style={styles.avatar} 
          source={{uri: User.photoUrl }}/>
          <Text  style={{fontWeight:'bold',fontSize:20}}> Changer la photo de profil</Text> 

          </TouchableOpacity>
          

          <Formik
         initialValues={{nom:nom ,prenom:prenom ,tel:tel,email:email ,password:password ,confirmpassword:confirmpassword }}
        onSubmit={(values, actions) => {
         alert(JSON.stringify(values));
        
          actions.setSubmitting(false);
          setNom(values.nom);
          setPrenom(values.prenom);
          setEmail(values.email);
          setTel(values.tel);
          setPassword(values.password);
          setConfirmpassword(values.confirmpassword);
         
        
        
      }}
      validationSchema={validationSchema}
    >
         {(formikProps) => (
               <React.Fragment>
          <View style={styles.inputContainer}>
          <Input 
           placeholder="Nom"  type="text"
           onChangeText={formikProps.handleChange('nom')}
           value={formikProps.values.nom}
           onBlur={formikProps.handleBlur('nom')}
           
           />
            <Text style={{ color: 'red' }}>{formikProps.errors.nom}</Text>
         <Input 
           placeholder="Prenom"  type="text"
           onChangeText={formikProps.handleChange('prenom')}
           value={formikProps.values.prenom}
           onBlur={formikProps.handleBlur('prenom')}
           
           />
      
       
       <Text style={{ color: 'red' }}>{formikProps.errors.prenom}</Text>
           <Input 
           placeholder="Email"  type="email"
           onChangeText={formikProps.handleChange('email')}
           value={formikProps.values.email}
           onBlur={formikProps.handleBlur('email')}
           
           />

       <Text style={{ color: 'red' }}>{formikProps.errors.email}</Text>
           <Input 
           placeholder="Tel"  type="text"
           onChangeText={formikProps.handleChange('tel')}
           value={formikProps.values.tel}
           onBlur={formikProps.handleBlur('tel')}
           
           />
       <Text style={{ color: 'red' }}>{formikProps.errors.tel}</Text>
            <Input 
             placeholder="Password"  type="password"
             onChangeText={formikProps.handleChange('password')}
             value={formikProps.values.password}
             secureTextEntry
             onBlur={formikProps.handleBlur('password')}
             />

        
       <Text style={{ color: 'red' }}>{formikProps.errors.password}</Text>
             <Input 
             placeholder="Confirme password"  type="password"
             onChangeText={formikProps.handleChange('confirmpassword')}
             value={formikProps.values.confirmpassword}
             secureTextEntry
             onBlur={formikProps.handleBlur('confirmpassword')}
             />
              <Text style={{ color: 'red' }}>{formikProps.errors.confirmpassword}</Text>
           </View>
           
                <Button
                containerStyle={styles.button}
                title="Sauvegarder"
                onPress={()=>navigation.navigate('Home')}
                type="outline"/>

              </React.Fragment>
                )}
            </Formik>
              
          </View>
         </ScrollView>    
        
       
    )
}

export default Profile

const styles = StyleSheet.create({

    container:{
        justifyContent:'center',
        alignItems:'center',
        marginTop:25,
        paddingTop:20,
       
       
    },
    inputContainer:{
        width:300,
        marginTop:35
    },
    button:{
        width:200,
        marginTop:30,
        color:"#955f31",
        marginBottom:10
        },
    avatar: {
            width: 130,
            height: 130,
           borderRadius: 63,
           borderWidth: 4,
           borderColor: "white",
           marginBottom:5,
           alignItems:'center',
           justifyContent:'center',
           marginLeft:25
          },

          profile:{
            flexDirection:'column', alignItems:'center',
            justifyContent:'center',
          }
        

})
