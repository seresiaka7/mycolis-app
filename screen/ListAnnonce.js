import React, { useState, useEffect } from "react";
import { Button, StyleSheet,Text,View,ActivityIndicator } from "react-native";
import { ListItem, Avatar } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import moment from "moment";
import { auth,db } from '../Firebase';

const ListAnnonce = (props,{navigation}) => {
  const [Trajet, setTrajet] = useState([]);
  const [Date, setDate] = useState("");
  const [isLoading,setIsLoading]=useState(true);

  useEffect(() => {
    db.collection("annonces").where("Iduser","==",auth.currentUser.uid).onSnapshot((querySnapshot) => {
      const Trajet = [];
      querySnapshot.forEach((doc) => {
        const {   date_depart,  image, ville_depart,ville_arrivee } = doc.data();
        Trajet.push({  
          id: doc.id,
          date_depart,  image, ville_depart,ville_arrivee
        });
      });
      setTrajet(Trajet);
      setDate(Trajet.date_depart);
      setIsLoading(false);
    });
  }, []);

  return (
    <ScrollView>
      
      <View >
      {

      isLoading ?(
      
      
        <View style={{ width:"100%",
        height:"100%",
        justifyContent:"center",
        alignItems:"center",
        alignContent:"center",
        backgroundColor:'#ECDDB6'}}> 
          <ActivityIndicator  size="large" color="#0000ff" />
         
        </View>
      
       
      )
      
      :(
            
        
      Trajet.map((trajet) => {
        // moment( setDate(trajet.date_depart));
        return (
          <ListItem
            key={trajet.id}
            bottomDivider
            onPress={() => {
              props.navigation.navigate("AnnonceDetail", {
                trajetId: trajet.id,
              });
            }}
          >
          
            <Avatar
              source={{
                uri:
                  trajet.image,
              }}
              rounded
            />
            <ListItem.Content>
              <ListItem.Title>{trajet.ville_depart} <Text>à</Text> {trajet.ville_arrivee}</ListItem.Title>
              <ListItem.Subtitle>{moment(Date).format('DD/MM/YYYY')}</ListItem.Subtitle>
            </ListItem.Content>
          </ListItem>
        );
      })
      )
    }
      
      </View>
    </ScrollView>
  );
};

export default ListAnnonce;
const styles = StyleSheet.create({

  load:{
  // flex:1,
   width:"100%",
   height:"100%",
   justifyContent:"center",
   alignItems:"center",
   alignContent:"center",
   backgroundColor:'#ECDDB6'
  },
  
  })
