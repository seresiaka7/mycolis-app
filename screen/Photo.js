import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button, Image,ScrollView ,TouchableOpacity} from 'react-native';
import { Camera } from 'expo-camera';
import * as ImagePicker from 'expo-image-picker';
import   Icon  from 'react-native-vector-icons/Ionicons';
import { auth,db } from '../Firebase';
import firebase from "firebase";
 require("firebase/firebase-storage");


export default function Photo({ navigation }) {
  const [hasGalleryPermission, setHasGalleryPermission] = useState(null);
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [camera, setCamera] = useState(null);
  const [image, setImage] = useState("https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png");
  const [type, setType] = useState(Camera.Constants.Type.back);

  useEffect(() => {
    (async () => {
      const cameraStatus = await Camera.requestPermissionsAsync();
      setHasCameraPermission(cameraStatus.status === 'granted');

      const galleryStatus = await ImagePicker.requestMediaLibraryPermissionsAsync();
      setHasGalleryPermission(galleryStatus.status === 'granted');


    })();   
  }, []);


  const uploadPhotoAsync= async (url)=>{
    const path=`photos/${auth.currentUser.uid}/${Date.now()}.jpg`
  return new Promise(async(res,rej)=>{
    const response=await fetch(url);
    const file= await response.blob();
    let upload = firebase.storage().ref(path).put(file);
    //db.storage().ref(path).put(file);
    upload.on(
      "state-changed",
      snapshot=>{},
      err=>{
       rej(err);
      },
      async ()=>{
        const url= await  upload.snapshot.ref.getDownloadURL();
        //upload.snapshot.ref.getDowloadURL();
        res(url);
       }
    );
  });
   };

  const onSave= async ()=>{
  
    const remoteUrl= await uploadPhotoAsync(image);
  
    db.collection("users").doc(auth.currentUser.uid).set({
      photoUrl:remoteUrl
  })

             
             .then((res)=> {  
               
             
              navigation.navigate('profile');})  
             .catch((error)=>
             console.log(error)
             ); 
             
             // 
             
  };
  
  

  const takePicture = async () => {
    if (camera) {
      const data = await camera.takePictureAsync(null);
      setImage(data.uri);
    }
  }

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 1,
    });
    console.log(result);

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };


  if (hasCameraPermission === null || hasGalleryPermission === false) {
    return <View />;
  }
  if (hasCameraPermission === false || hasGalleryPermission === false) {
    return <Text>No access to camera</Text>;
  }
  return (
   
    <View style={{flex:1 }} >
      <View style={styles.cameraContainer}>
        <Camera
          ref={ref => setCamera(ref)}
          style={styles.fixedRatio}
          type={type}
          ratio={'1:1'} />
      </View>
      <View style={styles.button}>  
     
      
      <TouchableOpacity   onPress={() => pickImage()} 
       style={styles.avatar} >
       {/*<Icon name="image"  size={25}></Icon>*/}
       {image && <Image source={{ uri: image || "https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png" }} style={styles.avatar} />}    

     </TouchableOpacity>

     <TouchableOpacity   onPress={() => {
          setType(
            type === Camera.Constants.Type.back
              ? Camera.Constants.Type.front
              : Camera.Constants.Type.back
          );
        }}  style={styles.avatar}>
     <Icon name="camera-reverse" size={25}></Icon>

     </TouchableOpacity>
     
    <TouchableOpacity   onPress={() => takePicture()}
       style={styles.avatar}   >
      <Icon name="add-circle" size={75} color="#955f31"></Icon>
      </TouchableOpacity>
    

     <TouchableOpacity  onPress={onSave}
       style={styles.btn}   >
       <Icon name="checkmark" size={45}   backgroundColor="blue"></Icon>
      </TouchableOpacity>
    
      </View>
    </View>
    
  );
}

const styles = StyleSheet.create({
  cameraContainer: {
    flex: 1,
    flexDirection:'row'
  },
  fixedRatio: {
    flex: 1,
    aspectRatio:0.7
  },
  button: {
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    margin:35,
    paddingTop:25,
    height:30,
    width:300,
   
    
   
  },
  avatar: {
    width: 75,
    height: 75,
   borderRadius: 63,  
   borderWidth: 4,
   borderColor: "white",
  
   alignItems:'center',
   justifyContent:'center',
   
  },
  btn:{
      
      padding:30,


  },
})