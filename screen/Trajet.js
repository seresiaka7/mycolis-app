import React,{useState} from 'react';
import {Modal, StyleSheet, Text,TouchableOpacity, View,Button,Platform, ScrollView ,TextInput,ActivityIndicator } from 'react-native';
import DateTimePicker from "@react-native-community/datetimepicker";
import {Picker} from '@react-native-picker/picker';
import moment from "moment";
import   Icon  from 'react-native-vector-icons/Ionicons';
import DateTimePickerModal from "react-native-modal-datetime-picker"   ;
import {Input,Image} from "react-native-elements";
import { Formik } from 'formik';
import * as yup from 'yup';
import { auth,db } from '../Firebase';
import { setStatusBarTranslucent } from 'expo-status-bar';



const Trajet = ({navigation}) => {
    const [Visible, setVisibility] = useState(false);
    const [VisibleAr, setVisibilityAr] = useState(false);
     const [date,setDate]=useState(moment());
     const [dateDisplay,setDateDisplay]=useState(moment());
     const [dateDisplayAr,setDateDisplayAr]=useState(moment());
     const [show,setShow]=useState(false);
     const [villeDep,setVilleDep]=useState("");
     const [villeAr,setVilleAr]=useState("");
     const [selectedLanguage, setSelectedLanguage] = useState();
     const [disable, setDisable] = useState(true);
   const onChange=(e,selectedDate) =>{
     setDateDisplay(moment(selectedDate));
   }
    
   const onPressCancel=()=>{
    setVisibility(false)
  }
  
 const onPressButton=()=>{
  setVisibility(true)
}
   
   const handleConfirm=(date)=>{
    setDateDisplay(date.toUTCString())
    setVisibility(false)
    setDisable(false)
 }
   const onChangeAr=(e,selectedDate) =>{
    setDateDisplayAr(moment(selectedDate));
  }



  const handleConfirmAr=(date)=>{
    setDateDisplayAr(date.toUTCString())
    setVisibilityAr(false)
    setDisable(false)
 }

 const onPressCancelAr=()=>{
  setVisibilityAr(false)
}

const onPressButtonAr=()=>{
setVisibilityAr(true)
}
 


const onTrajet= (val, act)=>{
  
  //alert(JSON.stringify(val));
       
  /*act.setSubmitting(false);
  
  setVilleDep(val.villeDep);
  setVilleAr(val.villeAr);
  setDateDisplay(val.date_depart);
  setDateDisplayAr(val.date_arrivee);*/
      db.collection("trajets")
      .add({
               Iduser:auth.currentUser.uid, 
               date_depart:dateDisplay,
               date_arrivee:dateDisplayAr, 
               ville_depart:villeDep,
               ville_arrivee:villeAr,
           })
           
           .then((res)=> {  
             
           
            navigation.navigate('ListTrajet');})  
           .catch((error)=>
           console.log(error)
           ); 
           
           // 
           
};

const validation = yup.object().shape({
 
//formikProps.handleSubmit
 //villeDep: yup.string().matches(phoneRegExp, "Numero de telephone  invalide").required("numero de telephone  8 chiffre").min(8,"Numero de telephone  8 chiffre"),

    villeDep: yup.string().required("champ obligatoire"),
    villeAr:  yup.string().required("champ obligatoire"),
   // date_depart:  yup.string().required("champ obligatoire"),
   // date_arrivee:  yup.string().required("champ obligatoire"),
  });
  

    return (

      <ScrollView  horizontal={false}  showsVerticalScrollIndicator={true} >
      <View style={styles.main}>
      
      <Formik
      initialValues={{villeDep:"",villeAr:"" , date_depart:"",
      date_arrivee:""}}
     
      onSubmit={(values, actions)=> {
       
          onTrajet(values,actions);
          actions.resetForm();
        
     }}
    
      
      validationSchema={validation}
     >
         {(formikProps) => (
               <React.Fragment>
           <View>

             <Text style={styles.txt}>  Pays de départ </Text>
            
      <View style={styles.picker}>

       <Picker
        selectedValue={villeDep}
         onValueChange={(itemValue, itemIndex) =>
         
         setVilleDep(itemValue)
        
        }>
       
        

        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="cote d'ivoire" value="cote d'ivoire" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="bresil" value="bresil" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="tunisie" value="tunisie" />
        <Picker.Item label="france" value="france" />
        </Picker>

        </View>





              
      </View>
      <View>

      <Text style={styles.txt}>  Pays d'arrivée </Text>

      <View style={styles.picker}>
      <Picker
        selectedValue={villeAr}
         onValueChange={(itemValue, itemIndex) =>
         
         setVilleAr(itemValue)
        
        }>
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="cote d'ivoire" value="cote d'ivoire" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="bresil" value="bresil" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
        <Picker.Item label="tunisie" value="tunisie" />
        <Picker.Item label="france" value="france" />
        </Picker>
        </View>  

      </View>
      
    
      <View>
             
             <Text style={styles.txt}>  Date de départ </Text>
             <TouchableOpacity
              activeOpacity={0}   onPress={onPressButton} >
             <Input
              style={styles.col}
              onFocus={onPressButton}
               onTextInput={onPressButton}
                placeholder='Entrez le prix'
                value={moment(dateDisplay).format('DD/MM/YYYY')}
                keyboardType="default"
               // onBlur={formikProps.handleBlur('date_depart')}
              rightIcon={
             <Icon
             name='calendar-outline'
              size={24}
             color="#204d48"
              />
              }
              onChangeText={(text)=>setDateDisplay(text)}

             />
            
              <DateTimePickerModal
              isVisible={Visible}
              onConfirm={handleConfirm}
              onCancel={onPressCancel}
              mode="date"
              onDateChange={onChange}
          />
           </TouchableOpacity>

          
      </View>
     
           
    
      <View>
             
             <Text style={styles.txt}> Date  de d'arrivée </Text>
             <TouchableOpacity
              activeOpacity={0}   onPress={onPressButtonAr} >
             <Input
              style={styles.col}
              onFocus={onPressButtonAr}
              onTextInput={onPressButtonAr}
                placeholder='Entrez le prix'
                value={moment(dateDisplayAr).format('DD/MM/YYYY')}
                keyboardType="default"
               // onBlur={formikProps.handleBlur('date_arrivee')}
            rightIcon={
             <Icon
             name='calendar-outline'
              size={24}
             color="#204d48"
              />
              }
              onChangeText={(text)=>setDateDisplayAr(text)}

             />
            
              <DateTimePickerModal
              isVisible={VisibleAr}
             onConfirm={handleConfirmAr}
              onCancel={onPressCancelAr}
              mode="date"
             onDateChange={onChangeAr}
          />
           </TouchableOpacity>
         
          
      </View>
     


      {formikProps.isSubmitting ? (
            <ActivityIndicator  size="large" color="#0000ff" />
          ) : (
     <View  style={styles.save}>
         <TouchableOpacity onPress={onTrajet}  disabled={false }>
             <Icon
             name='checkmark'
              size={70}
             color="white"
             style={styles.iconsave}
              />
         </TouchableOpacity>
        
<Text> {villeDep}</Text>
     </View>
     )}
     </React.Fragment>
         )}
      </Formik>
       </View>
       </ScrollView>
    );
  };

export default Trajet

const styles = StyleSheet.create({

  main:{
   marginTop:50, 
   //backgroundColor:'#ECDDB6',
  },
  picker:{
 borderBottomWidth:1,
 marginHorizontal:7,
 marginBottom:10,
 borderColor:'gray'
  },
  txt:{
    fontSize:17 ,
   },
   save:{
    justifyContent:'flex-end' ,
    alignItems:'center',
    marginBottom:10,
    marginRight:4,
    marginTop:30,
   },
   iconsave:{
    width: 75,
    height: 75,
   borderRadius: 63,  
   borderWidth: 4,
   borderColor: "#955f31",
   backgroundColor: "#955f31",
    
   }, 
   img:{
    fontSize:17 ,
    marginTop:20,
    marginLeft:5,
    color:'gray',
   },
   col:{
    color:'gray', 
   
   },
   textarea:{
      marginBottom:10,
     
   },
   textinput:{
    marginLeft:5,
   },
in:{
    justifyContent:'center',
    alignItems:'center',
   
    marginTop:45,
},
btnText:{
  position:'absolute',
  top:0,
  height:42,
  paddingHorizontal:20,
  flexDirection:'row',
  alignItems:'center',
  justifyContent:'center',

},
btnCancel:{ 
  left:0
},
btnDone:{
  right:0,
}

})
/*


const onTrajet= (val, act)=>{
  
  alert(JSON.stringify(val));
       
  act.setSubmitting(false);
  
  setVilleDep(val.villeDep);
  setVilleAr(val.villeAr);
  setDateDisplay(val.date_depart);
  setDateDisplayAr(val.date_arrivee);
      db.collection("trajets")
      .add({
               Iduser:auth.currentUser.uid, 
               date_depart:dateDisplay,
               date_arrivee:dateDisplayAr, 
               ville_depart:villeDep,
               ville_arrivee:villeAr,
           })
           
 <Text style={{ color: 'red' }}>{formikProps.errors.villeAr}</Text>

 <Text style={styles.txt}>  Ville d'arrivée </Text>
              <Input 
               placeholder="Entrez la ville d'arrivée"  type="text"
               onChangeText={formikProps.handleChange('villeAr')}
               value={formikProps.values.villeAr}
               onBlur={formikProps.handleBlur('villeAr')}
               />
 <Input 
              placeholder="Entrez la ville "  type="text"
              onChangeText={formikProps.handleChange('villeDep')}
              value={formikProps.values.villeDep}

              onBlur={formikProps.handleBlur('villeDep')}
              
              />
 onChangeText={(text)=>setDateDisplayAr(text)}

 <View>
     
        <TouchableOpacity
            activeOpacity={0}   onPress={onPressButton} >
     
         <View style={{marginHorizontal:45}}  onPress={onPressButton}>
             <View style={{paddingVertical:15,paddingHorizontal:10,borderColor:'gray',borderWidth:1 ,marginTop:70,flexDirection:'row' }}><Text>{moment(dateDisplay).format('DD/MM/YYYY')} </Text>
              <Icon name="calendar-outline" size={25} style={{marginLeft:100,paddingLeft:44}}  color="#204d48"></Icon>
            </View>
       
         </View>
         <DateTimePickerModal
           isVisible={Visible}
          onConfirm={handleConfirm}
          onCancel={onPressCancel}
           mode="date"
          onDateChange={onChange}
          />
     
       </TouchableOpacity>
      </View>











  <TouchableOpacity
         activeOpacity={0} onPress={()=>setShow(true)}>
     
      <View style={{marginHorizontal:45}}>
        <Text style={{paddingVertical:15,paddingHorizontal:10,borderColor:'gray',borderWidth:1 ,marginTop:70 }}>{date.format('MMMM Do, YYYY')}</Text>
      </View>
      <Modal
      transparent={true}  
      animationType='slide'
      visible={show}
      supportedOrientations={['portrait']}
      onRequestClose={()=>setShow(false)}
      >
        <View style={{flex:1}}>
         <TouchableOpacity style={{flex:1,alignItems:'flex-end',flexDirection:'row'}} activeOpacity={1} visible={show} onPress={()=>setShow(false)}>
      <TouchableOpacity underlayColor={'#fff'} style={{flex:1,borderTopColor:'#e9e9e9',borderTopWidth:1}}
      onPress={()=>console.log('datepicker clicked')}
      >
   <View style={{backgroundColor:'#fff',height:256,overflow:'hidden'}}>
   <View style={{marginTop:20}}>
     <DateTimePicker
      timeZoneOffsetInMinutes={0}
      value={new Date(date)}
      mode='date'
      minimumDate={new Date(moment().subtract(120,'years').format('YYYY-MM-DD'))}
      maximumDate={new Date(moment().format('YYYY-MM-DD'))}
      onChange={onChange}
     />
   </View>
     <TouchableOpacity underlayColor={'transparent'} onPress={onCancelPress}
     style={[styles.btnText,styles.btnCancel]}
      >
    <Text>ok </Text>
   </TouchableOpacity>

   <TouchableOpacity underlayColor={'transparent'} onPress={onCancelDone}
     style={[styles.btnText,styles.btnDone]}
      >
 <Text> Done</Text>
   </TouchableOpacity>
   </View>

      </TouchableOpacity>
         </TouchableOpacity>

        </View>


      </Modal>
       </TouchableOpacity>
*/ 