import React,{useState} from 'react';
import {StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity
  } from 'react-native';
import {Button,Input} from "react-native-elements";
import   Icon  from 'react-native-vector-icons/Ionicons';

const Dashboard = ({navigation}) => {

 
    return (
      <View style={styles.container}>
        <View style={styles.mess}>
       <TouchableOpacity  
       style={styles.btn} >
      <Icon name="notifications-outline" size={30}></Icon>
       </TouchableOpacity>

       <TouchableOpacity  
       style={styles.btn} >
       <Icon name="mail-unread-outline" size={30}></Icon>
       </TouchableOpacity>
        </View>

          <View style={styles.header}></View>


          <Image style={styles.avatar} source={{uri: "https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png"}}/>
          <View style={styles.body}>
            <View style={styles.bodyContent}>
              <Text style={styles.name}>Sere Siaka </Text>
            
              <Button
                 containerStyle={styles.buttPro}
                title="Profil"
               
                onPress={()=>navigation.navigate('profile')}
                />
              <View style={styles.buttonContainer}>
              

             
                <Button
                containerStyle={styles.button}
                title=" Annonces"
                onPress={()=>navigation.navigate('ListAnnonce')}
                type="outline"
                />
              
                <Button
                 containerStyle={styles.button}
                title="Trajet"
                type="outline"
                onPress={()=>navigation.navigate('ListTrajet')}
                />
              </View>
           </View>
           <TouchableOpacity    onPress={()=>navigation.navigate('choose')}
             style={styles.new}   >
             <Icon name="add-circle" size={75} color="#204d48"></Icon>
           </TouchableOpacity>
        </View>
      </View>
    );
  }

  export default Dashboard

const styles = StyleSheet.create({
  header:{
    backgroundColor: "#fff",
    height:200,
    
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom:10,
    alignSelf:'center',
    position: 'absolute',
    marginTop:130
  },
  
  body:{
    marginTop:40,
   
  },
  bodyContent: {
   justifyContent:'center',
    alignItems: 'center',
    padding:15,
  },
  name:{
    fontSize:28,
    color: "#696969",
    fontWeight: "600"
  },
  info:{
    fontSize:16,
    color: "#00BFFF",
    marginTop:10
  },
  
  buttonContainer:{
      flexDirection:'row',
      marginTop:25
  },
  button:{
    width:125,
    marginLeft:10,
    },
    buttPro:{
      width:258,
      marginLeft:10,
      marginTop:20,
      },
    mess:{
      flexDirection:'row',
      alignItems:'flex-end',
      justifyContent:'flex-end'
    },
    btn:{
      margin:5,
    },
    new:{
      alignItems:'center',
      marginTop:40,

    }
 
}
)
