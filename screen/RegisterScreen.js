import React,{useLayoutEffect, useState,useEffect} from 'react';
import { StyleSheet, Text, View,KeyboardAvoidingView,ScrollView,ActivityIndicator } from 'react-native';
import {Button,Input,Image} from "react-native-elements";
import { StatusBar } from 'expo-status-bar';
import { auth ,db} from '../Firebase';
import { Formik } from 'formik';
import * as Yup from 'yup';


const RegisterScreen = ({navigation}) => {

    const [nom,setNom]= useState("");
    const [prenom,setPrenom]= useState("");
    const [tel,setTel]= useState("");
    const [email,setEmail]= useState("");
    const [password,setPassword]= useState("");
    const [confirmpassword,setConfirmpassword]= useState("");

    useLayoutEffect(()=>{
        navigation.setOptions({
         headerBackTitle:"Back to login" ,  
        });
    },[navigation]);

const register= async (values,actions)=>{

  try {
  actions.setSubmitting(false);
  setNom(values.nom);
  setPrenom(values.prenom);
  setEmail(values.email );
  setTel(values.tel);
  setPassword(values.password);
  setConfirmpassword(values.confirmpassword);
   await auth.createUserWithEmailAndPassword(email,password);
              
             
              db.collection("users").doc(auth.currentUser.uid).set({
                nom:nom,
                prenom:prenom,
                email:email,
                telephone:tel,
                password:password
            })
          
           // console.log(res);
           //navigation.push({   id: 'Login' })
           navigation.navigate('Login');
        
        }
        catch(error){
            console.log(error)
           
        }
};
const lregex=/(?=.*[a-z])/;
const uregex=/(?=.*[A-Z])/;
const nregex=/(?=.*[0-9])/;
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const validation= Yup.object().shape({
    nom: Yup.string().required().label('nom'),
    prenom: Yup.string().required().label('prenom'),
   
    tel: Yup.string().matches(phoneRegExp, "Numero de telephone  invalide").required("Numero de telephone  à 8 chiffre").min(8,"Numero de telephone  8 chiffre"),

    email: Yup.string().lowercase().email('email doit être valide').required('email obligatoire'),
    password: Yup.string().matches(lregex,"une lettre miniscule est obligatoire").matches(uregex,"une lettre majuscule  est obligatoire").matches(nregex,"une valeur  est obligatoire").required('password obligatoire').min(8,"8 caractères au minimum"),
    confirmpassword: Yup.string().oneOf([Yup.ref('password')],"les mots de passe doivent être egaux").required("password obligatoire")
    ,
  });

    return (
       
      <ScrollView  horizontal={false}  showsVerticalScrollIndicator={true} >
        <View   style={styles.container}>
        
        <StatusBar style="light"/>
        <Text style={{fontSize:20,marginTop:15, marginBottom:10,fontWeight:'bold',color:'gray'}}> Créer Compte </Text>

        <Formik
        initialValues={{nom:"" ,prenom:"" ,tel:"",email:"" ,password:"" ,confirmpassword:"" }}
        onSubmit={(values, actions) => {
       // alert(JSON.stringify(values));
        
         
          register(values,actions);
        
        
      }}
      validationSchema={validation}
    >
         {(formikProps) => (
               <React.Fragment>

        <View style={styles.inputContainer}>

        <Input 
           placeholder="Nom" 
           onChangeText={formikProps.handleChange('nom')}
           value={formikProps.values.nom}
           onBlur={formikProps.handleBlur('nom')}
           keyboardType="default"
           />
            <Text style={{ color: 'red' }}>{formikProps.errors.nom}</Text>
         <Input 
           placeholder="Prenom"  
           onChangeText={formikProps.handleChange('prenom')}
           value={formikProps.values.prenom}
           onBlur={formikProps.handleBlur('prenom')}
           keyboardType="default"
           />
      
       
       <Text style={{ color: 'red' }}>{formikProps.errors.prenom}</Text>
           <Input 
           placeholder="Email"  
           onChangeText={formikProps.handleChange('email')}
           value={formikProps.values.email.trim()}
           onBlur={formikProps.handleBlur('email')}
           keyboardType="email-address"
           />

       <Text style={{ color: 'red' }}>{formikProps.errors.email}</Text>
           <Input 
           placeholder="Tel"
           onChangeText={formikProps.handleChange('tel')}
           value={formikProps.values.tel}
           onBlur={formikProps.handleBlur('tel')}
           keyboardType="phone-pad"
           />
       <Text style={{ color: 'red' }}>{formikProps.errors.tel}</Text>
          <Input
             placeholder="Password" 
             onChangeText={formikProps.handleChange('password')}
             value={formikProps.values.password}
             secureTextEntry
             onBlur={formikProps.handleBlur('password')}
             keyboardType="default"
             />

        
       <Text style={{ color: 'red' }}>{formikProps.errors.password}</Text>
             <Input 
             placeholder="Confirme password" 
             onChangeText={formikProps.handleChange('confirmpassword')}
             value={formikProps.values.confirmpassword}
             secureTextEntry
             onBlur={formikProps.handleBlur('confirmpassword')}
             keyboardType="default"
             />
              <Text style={{ color: 'red' }}>{formikProps.errors.confirmpassword}</Text>
        </View>
        
      {formikProps.isSubmitting ? (
            <ActivityIndicator />
          ) : (
        <Button
             containerStyle={styles.button}
             title="register"
             onPress={formikProps.handleSubmit}
             />
             )}
             <Button
             containerStyle={styles.button}
             title="Connecter"
             onPress={()=>navigation.navigate('home')}
             type="outline"
             />
           </React.Fragment>
         )}
      </Formik>
     </View >
     </ScrollView>
    )
}

export default RegisterScreen

const styles = StyleSheet.create({

    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        padding:10,
        backgroundColor:"white",
      },
      inputContainer:{
          width:300,
      },
  button:{
  width:200,
  marginTop:10,
  },


})
