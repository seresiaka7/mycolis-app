import React,{ useState,useEffect} from 'react';
import { StyleSheet, Text, View, Alert,TouchableOpacity,ActivityIndicator } from 'react-native';
import {Button,Input,Image} from "react-native-elements"; 
import { StatusBar } from 'expo-status-bar';
import { auth } from '../Firebase';
import   Icon  from 'react-native-vector-icons/Ionicons';
import { Formik } from 'formik';
import * as yup from 'yup';
import * as Facebook from 'expo-facebook';
import * as GoogleSignIn from 'expo-google-sign-in';
import * as Google from 'expo-google-app-auth';






const LoginScreen = ({navigation}) => {

    const [email,setEmail]= useState("");
    const [password,setPassword]= useState("");
    const [user,setUser]= useState(null);
    const [mail,setMail]= useState("");
    const [name,setName]= useState("");
    const [photoUrl,setPhotoUrl]= useState("");
   /* useEffect(()=>{
        const unsubscribe= auth.onAuthStateChanged((authUser)=>{
           console.log(authUser);
           if(authUser){
                
                 navigation.replace('Accueil',authUser); 
                 
    
             }
         });
         return unsubscribe;
         },[]);*/
    
        
      
     

        const   GoogleLogin=()=> {
          
          const config=  {
              // You may ommit the clientId when the firebase `googleServicesFile` is configured
              clientId: "971262823188-jkhnflv2kbrgn45uvk2rpu7eqo68t4u5.apps.googleusercontent.com",
               scopes:['profile','email']
            }

           Google
           .logInAsync(config)
           .then((res)=>{
             const {type,user}=res;
             if(type=="success"){
                 
              const {email,name,photoUrl}=user
              //navigation.navigate('home', { screen: ('Accueil',{name,email,photoUrl}) });
              navigation.navigate('home',{
                screen: 'Home',
                params:{email,name,photoUrl}});
             // navigation.navigate('home', { screen: 'Accueil' });
             }
             else{  
               alert("Google signin was cancelled");
             }
           }
            )
            .catch ((err )=> {
            alert('login  Error:' + err)
           })
           };










         async function logIn() {
          try {
            await Facebook.initializeAsync({
              appId: '305507384569907',
            });
            const {
              type,
              token,
              nom,
              expirationDate,
              permissions,
              declinedPermissions,
            } = await Facebook.logInWithReadPermissionsAsync({
              permissions: ['public_profile'],
            });
            if (type === 'success') {
              // Get the user's name using Facebook's Graph API
             
           const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
         
            Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
           // const nom= response.json().name;
           navigation.replace('home', { screen: 'Home'});
           //params:{name:(await response.json()).name}})
             
            } else {
              // type === 'cancel'
            }
          } catch ({ message }) {
            alert(`Facebook Login Error  sere : ${message}`);
          }
        }

        /* const credential=auth.FacebookAuthProvider.credential(token);
              auth.signInWithCredential(credential).catch(error=>{
                Console.log(error);
            });*/
             // https://mycolis-1fe33.firebaseapp.com/__/auth/handler
    const onSignin=()=>{
        
         auth
             .signInWithEmailAndPassword(email,password)  
             .then((res)=> {navigation.replace('home', { screen: 'Accueil' });} )  
              .catch((error)=>alert(error));
    }  


    const lregex=/(?=.*[a-z])/;
    const uregex=/(?=.*[A-Z])/;
    const nregex=/(?=.*[0-9])/;
    const validationSchema = yup.object().shape({
        email: yup.string().required('email obligatoire').email('email doit être valide '),
        password: yup.string().matches(lregex,"une lettre miniscule est obligatoire").matches(uregex,"une lettre majuscule  est obligatoire").matches(nregex,"une valeur  est obligatoire").required('password obligatoire').min(8,"8 caractères au minimum"),
      });

      /*
        <StatusBar style="light"/>
           <Image
           source={{ uri:"https://www.gpslink.co.uk/static/assets/img/login.png"}}
           style={{width:200,height:200 }}
           />


      */
    return (

      <View  style={styles.container}>
         
    <Formik
      initialValues={{email:email,password:password }}
      onSubmit={(values, actions) => {
        alert(JSON.stringify(values));
       
         setEmail(values.email);
         setPassword(values.password);
          actions.setSubmitting(false);
          onSignin();
      }}
      validationSchema={validationSchema}
       >
        
         {(props) => (
               
               <React.Fragment>
           <View style={styles.inputContainer}>
              <Input 
              placeholder="Email"  
              value={props.values.email}
              onChangeText={props.handleChange('email')}
              onBlur={props.handleBlur("email")}
              keyboardType="email-address"
              />
               <Text style={{ color:'red' }}>{props.errors.email}</Text>
               <Input 
                placeholder="Password" 
                value={props.values.password}
                onChangeText={props.handleChange('password')}
                secureTextEntry
                onSubmitEditing={onSignin}
                onBlur={props.handleBlur("password")}
                keyboardType="default"
                />
                 <Text style={{ color:'red'}}>{props.errors.password}</Text>
           </View>
           
           {props.isSubmitting ? (
            <ActivityIndicator />
          ) : (
            <Button
                containerStyle={styles.button}
                title="Sign In"
                onPress={props.handleSubmit}
                />
          )}

          </React.Fragment>
          )}
       </Formik>
                <Button
                containerStyle={styles.button}
                title="facebook"
                onPress={logIn}
                type="outline"
                />

               <Button
                containerStyle={styles.button}
                title="google"
                onPress={GoogleLogin}
                type="outline"
                />
             <View
                style={styles.buttonsss}
                >
              <Text>Don't have an account already?</Text> 
              <TouchableOpacity  onPress={()=>navigation.navigate('register')}>
                <Text style={{color:'blue'}}> Signup</Text>
              </TouchableOpacity>
             </View>
        
        
       
                
        </View >
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
      flex:1,
      alignItems:"center",
      justifyContent:"center",
      padding:10,
      //margin:,
      backgroundColor:"white",
    },
    inputContainer:{
        width:300,
    },
button:{
width:200,
marginTop:10,
},

})
