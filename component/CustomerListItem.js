import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Avatar,ListItem } from 'react-native-elements'

const CustomerListItem = () => {
    return (
       <ListItem>
       <Avatar
       rounded
       source={{
           uri: "https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-4.png"
       }}
       />

    <ListItem.Content>
        <ListItem.Title style={{fontWeight:"800"}}>
            Youtube Chat
        </ListItem.Title>
        <ListItem.Subtitle
        numberOfLines={1}
        ellipsizeMode="tail"
        >
            this is test subtitle

        </ListItem.Subtitle>
    </ListItem.Content>
       </ListItem>
    )
}

export default CustomerListItem

const styles = StyleSheet.create({})
